import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:untitled1/core/errors/exceptions/server_exception.dart';

dynamic handleRequestResponse(http.Response response) {
  switch (response.statusCode) {
    case 400:
      throw BadRequestException(response.body.toString());
    case 401:
      throw UnauthorisedException(response.body);
    case 403:
      throw UnauthorisedException(response.body.toString());
    case 422:
      throw BadRequestException(response.body);
    case 500:
      throw FetchDataException(
          message:
              'Error occured while Communication with Server with StatusCode: ${response.statusCode}');
    default:
      throw FetchDataException(
          message:
              'Error occured while Communication with Server with StatusCode: ${response.statusCode}');
  }
}
