class ServerException implements Exception{

}

class NotConnectedException implements Exception{}

class CustomException implements Exception {
  final _message;

  CustomException([this._message]);

  @override
  String toString() {
    return  _message;
  }
}

class FetchDataException extends CustomException {
  FetchDataException({required String message})
      : super(message);
}

class BadRequestException extends CustomException {
  BadRequestException([message]) : super(message);
}

class UnauthorisedException extends CustomException {
  UnauthorisedException([message]) : super(message);
}

class InvalidInputException extends CustomException {
  InvalidInputException({required String message}) : super(message,);
}