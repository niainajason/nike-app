import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  bool isFailed = true;
  String message = "";

  Failure({required this.message, required this.isFailed});
  @override
  List<Object?> get props => [isFailed, message];
}

class ServerFailure extends Failure {
  @override
  final bool isFailed;
  ServerFailure({required String message, this.isFailed = true})
      : super(message: message, isFailed: true);
}

class CacheFailure extends Failure {
  CacheFailure() : super(message: "Erreur de cache", isFailed: true);
}

class NotConnectedFailure extends Failure {
  NotConnectedFailure() : super(message: "Non connecté", isFailed: true);
}
