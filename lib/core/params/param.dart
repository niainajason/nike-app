import 'package:equatable/equatable.dart';

class NoParam extends Equatable {
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class IdParam extends Equatable {
  final int id;
  const IdParam(this.id);

  @override
  // TODO: implement props
  List<Object?> get props => [id];
}

class StringParam extends Equatable {
  final String value;
  const StringParam({required this.value});

  @override
  // TODO: implement props
  List<Object?> get props => [value];
}

class ObjectParam extends Equatable {
  final Object? value;
  const ObjectParam(this.value);
  @override
  // TODO: implement props
  List<Object?> get props => [value];
}

class GenderParam extends Equatable {
  final String value;
  const GenderParam({required this.value});

  @override
  // TODO: implement props
  List<Object?> get props => [value];
}
