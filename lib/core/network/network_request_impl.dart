import 'dart:convert';
import 'dart:io';
import 'dart:developer' as developer;

import 'package:http/http.dart' as http;
import 'package:untitled1/core/network/network_request.dart';

class NetworkRequestImpl implements NetworkRequest {
  final http.Client client;
  NetworkRequestImpl({required this.client});

  @override
  Future<http.Response> getData({url, token}) async {
    return await client.get(
      Uri.parse(url),
      headers: setHeader(token: token),
    );
  }

  @override
  Future<http.Response> postData({params, url, token}) async {
    return await client.post(
      Uri.parse(url),
      body: params,
      headers: setHeader(token: token),
    );
  }

  @override
  Future<http.Response> putData({params, url, token}) async {
    return await client.put(
      Uri.parse(url),
      body: params,
      headers: setHeader(token: token),
    );
  }

  @override
  Map<String, String>? setHeader({token}) {
    Map<String, String> header;
    header = {
      "Accept": "application/json",
      'Content-Type': 'application/json',
    };
    if (token != null) {
      header["Authorization"] = "Bearer $token";
    }
    return header;
  }

  @override
  Future<http.Response> getDataWithBody({params, url, token}) {
    // TODO: implement getDataWithBody
    throw UnimplementedError();
  }
}
