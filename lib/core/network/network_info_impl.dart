import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:untitled1/core/network/network_info.dart';

class NetworkInfoImpl implements NetworkInfo {
  final InternetConnectionChecker checker;
  NetworkInfoImpl({required this.checker});
  @override
  // TODO: implement isOnline
  Future<bool> get isOnline => checker.hasConnection;
}
