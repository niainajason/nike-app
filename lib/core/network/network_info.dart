abstract class NetworkInfo{
  Future<bool> get isOnline;
}