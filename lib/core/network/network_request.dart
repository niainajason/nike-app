import 'package:http/http.dart' as http;

abstract class NetworkRequest{
  Map<String, String>? setHeader({token});
  Future<http.Response> getData({url, token});
  Future<http.Response> getDataWithBody({params, url, token});
  Future<http.Response> postData({params, url, token});
  Future<http.Response> putData({params, url, token});
}