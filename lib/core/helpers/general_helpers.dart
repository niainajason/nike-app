import 'dart:developer' as developer;

class Helper {
  List? convertStringToList(String? value) {
    List? items = value?.split(",");
    return items;
  }

  double convertPriceToDouble(price) {
    late double priceValue;
    if (price.isNotEmpty) {
      price = price.replaceAll(" €", "");
      priceValue = double.parse(price.replaceAll(",", "."));
    } else {
      priceValue = 0.0;
    }
    return priceValue;
  }
}
