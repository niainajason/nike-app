import 'package:multiple_result/multiple_result.dart';
import 'package:untitled1/core/errors/failure.dart';

abstract class Usecase<Type, Params> {
  Future<Result<Type, Failure>> call(Params params);
}
