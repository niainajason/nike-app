import 'dart:convert';

import 'package:untitled1/core/constants/data.dart';
import 'package:untitled1/core/errors/exceptions/server_exception.dart';
import 'package:untitled1/data/datasources/local/shoes_source_locale.dart';
import 'package:untitled1/data/models/shoes_model.dart';
import 'dart:developer' as developer;

class ShoesSourceLocalImpl implements ShoesSourceLocale {
  ShoesSourceLocalImpl({shoesModel});

  @override
  Future<List<ShoesModel>> getShoesList(Object params) {
    print(params);
    try {
      List<ShoesModel> shoesModelData = [];
      List<dynamic> result = shoesList;
      for (var item in result) {
        if (item['article'] != '') {
          shoesModelData.add(ShoesModel.fromJson(item));
        }
      }
      return Future.value(shoesModelData);
    } on ServerException {
      throw ServerException();
    }
  }
}
