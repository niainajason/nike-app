import 'package:untitled1/data/models/shoes_model.dart';

abstract class ShoesSourceLocale {
  Future<List<ShoesModel>> getShoesList(Object params);
}
