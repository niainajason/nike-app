import 'package:untitled1/core/helpers/general_helpers.dart';
import 'package:untitled1/domain/entities/shoes_entity.dart';

class ShoesModel extends ShoesEntity {
  ShoesModel(
      {final String? article,
      final double? price,
      final String? gender,
      final String? sport,
      final String? image,
      final String? color})
      : super(
            article: article,
            price: price,
            gender: gender,
            sport: sport,
            image: image,
            color: color);

  factory ShoesModel.fromJson(Map<String, dynamic> jsonMap) {
    return ShoesModel(
        article: jsonMap['article'],
        price: Helper().convertPriceToDouble(jsonMap['prix']),
        gender: jsonMap['sexe'],
        sport: jsonMap['sport'],
        image: jsonMap['photo'],
        color: jsonMap['couleur']);
  }

  Map<String, dynamic> toJson() {
    return {
      'article': article,
      'price': price,
      'gender': gender,
      'sport': sport,
      'image': image,
      'color': color
    };
  }
}
