import 'package:multiple_result/src/result.dart';
import 'package:untitled1/core/errors/failure.dart';
import 'package:untitled1/data/datasources/local/shoes_source_locale.dart';
import 'package:untitled1/domain/entities/shoes_entity.dart';
import 'package:untitled1/domain/repositories/shoes_repository.dart';
import 'dart:developer' as developer;

class ShoesRepositoryImpl implements ShoesRepository {
  final ShoesSourceLocale shoesSourceLocale;
  ShoesRepositoryImpl({required this.shoesSourceLocale});
  @override
  Future<Result<List<ShoesEntity>, Failure>> getListShoes(Object param) async {
    try {
      var result = await shoesSourceLocale.getShoesList(param);
      return Success(result);
    } catch (e) {
      throw Error(ServerFailure(message: e.toString()));
    }
  }

  // Result<Exception, String> getSomethingPretty() {
  //   if (1 < 0) {
  //     return Success('OK!');
  //   } else {
  //     return Error(Exception('Not Ok!'));
  //   }
  // }
}
