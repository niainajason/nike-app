import 'package:get_it/get_it.dart';
import 'package:untitled1/data/datasources/local/shoes_source_locale.dart';
import 'package:untitled1/data/datasources/local/shoes_source_locale_impl.dart';
import 'package:untitled1/data/repositories/shoes_repository_impl.dart';
import 'package:untitled1/domain/repositories/shoes_repository.dart';
import 'package:untitled1/domain/usecases/shoes/get_list_usecase.dart';
import 'package:untitled1/presentation/filters/bloc/filter_bloc.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_bloc.dart';

final getIt = GetIt.instance;
Future init() async {
  getIt.registerFactory(() => ShoesBloc(getListUseCase: getIt()));
  getIt.registerFactory(() => FiltersBloc());

  //repository
  getIt.registerLazySingleton<ShoesRepository>(
      () => ShoesRepositoryImpl(shoesSourceLocale: getIt()));

  // usercase
  getIt.registerLazySingleton(() => GetListUseCase(repository: getIt()));

  // Data
  getIt.registerLazySingleton<ShoesSourceLocale>(() => ShoesSourceLocalImpl());

  // Service externe
}
