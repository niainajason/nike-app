import 'package:multiple_result/multiple_result.dart';
import 'package:untitled1/core/errors/failure.dart';
import 'package:untitled1/core/usecase/usecase.dart';
import 'package:untitled1/domain/entities/shoes_entity.dart';
import 'package:untitled1/domain/repositories/shoes_repository.dart';

class GetListUseCase implements Usecase<List<ShoesEntity>, Object> {
  final ShoesRepository? repository;
  GetListUseCase({this.repository});

  @override
  Future<Result<List<ShoesEntity>, Failure>> call(Object shoesEntity) async {
    return await repository!.getListShoes(shoesEntity);
  }
}
