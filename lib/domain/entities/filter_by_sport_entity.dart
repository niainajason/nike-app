import 'package:equatable/equatable.dart';

class FilterBySportEntity extends Equatable {
  final String? title;
  final String? value;
  bool isChecked = false;

  FilterBySportEntity({this.title, this.value, required this.isChecked});

  @override
  // TODO: implement props
  List<Object?> get props => [title, value, isChecked];
}
