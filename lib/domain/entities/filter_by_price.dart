import 'package:equatable/equatable.dart';

class FilterByPriceEntity extends Equatable {
  final String? title;
  final List? price;
  bool isChecked = false;

  FilterByPriceEntity({this.title, this.price, required this.isChecked});

  @override
  // TODO: implement props
  List<Object?> get props => [title, price, isChecked];
}
