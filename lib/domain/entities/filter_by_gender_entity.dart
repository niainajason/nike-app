import 'package:equatable/equatable.dart';

class FilterByGenderEntity extends Equatable {
  final String? title;
  final String? value;
  bool isChecked = false;

  FilterByGenderEntity({this.title, this.value, required this.isChecked});

  @override
  // TODO: implement props
  List<Object?> get props => [title, value, isChecked];
}
