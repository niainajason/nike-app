import 'package:equatable/equatable.dart';

class FilterByColorEntity extends Equatable {
  final String? title;
  final String? color;
  final String? value;
  bool isChecked = false;

  FilterByColorEntity(
      {this.title, this.color, this.value, required this.isChecked});

  @override
  // TODO: implement props
  List<Object?> get props => [title, color, value, isChecked];
}
