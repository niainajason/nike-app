import 'package:equatable/equatable.dart';

class ShoesEntity extends Equatable {
  final String? article;
  final double? price;
  final String? gender;
  final String? sport;
  final String? image;
  final String? color;

  const ShoesEntity(
      {this.article,
      this.price,
      this.gender,
      this.sport,
      this.image,
      this.color});

  @override
  // TODO: implement props
  List<Object?> get props => [article, price, gender, sport, image, color];
}
