import 'package:multiple_result/multiple_result.dart';
import 'package:untitled1/core/errors/failure.dart';
import 'package:untitled1/domain/entities/shoes_entity.dart';

abstract class ShoesRepository {
  Future<Result<List<ShoesEntity>, Failure>> getListShoes(Object param);
}
