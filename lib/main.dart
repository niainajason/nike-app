import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/injections.dart';
import 'package:untitled1/presentation/filters/bloc/filter_bloc.dart';
import 'package:untitled1/presentation/filters/bloc/filters_event.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_bloc.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_event.dart';
import 'package:untitled1/presentation/views/wrapper/pages/filter_mobile.dart';
import 'package:untitled1/presentation/views/wrapper/pages/wrapper_page.dart';

void main() async {
  await init();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => getIt<ShoesBloc>()..add(ShoesInit())),
        BlocProvider(
            create: (context) =>
                getIt<FiltersBloc>()..add(OnFilterByGenderInit())),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: const MainPageWrapper(),
        routes: {
          '/dashboard': (context) => const MainPageWrapper(),
          '/filterMobile': (context) => const FilterMobile()
        },
        onGenerateRoute: (settings) {
          return MaterialPageRoute(
              builder: (context) => const MainPageWrapper());
        },
        navigatorKey: navigatorKey,
      ),
    );
  }
}
