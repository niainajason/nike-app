import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:untitled1/presentation/filters/bloc/filter_bloc.dart';
import 'package:untitled1/presentation/filters/bloc/filters_event.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_bloc.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_event.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_state.dart';
import 'package:untitled1/presentation/shoes/page/shoes_list.dart';
import 'package:untitled1/presentation/views/widgets/filter_by_color.dart';
import 'package:untitled1/presentation/views/widgets/filter_by_gender.dart';
import 'package:untitled1/presentation/views/widgets/filter_by_price.dart';
import 'package:untitled1/presentation/views/widgets/filter_by_sport.dart';
import 'package:untitled1/presentation/views/widgets/shoesItem.dart';

class MainPageWrapper extends StatefulWidget {
  const MainPageWrapper({Key? key}) : super(key: key);

  @override
  State<MainPageWrapper> createState() => _MainPageWrapperState();
}

class _MainPageWrapperState extends State<MainPageWrapper> {
  var isWeb = kIsWeb;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () async {
        exit(0);
      },
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: isWeb
                  ? (screenSize <= 600)
                      ? 0
                      : MediaQuery.of(context).size.width / 8
                  : 0,
              vertical: 2.0),
          child: SafeArea(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: isWeb
                        ? const EdgeInsets.symmetric(horizontal: 00.0)
                        : const EdgeInsets.symmetric(horizontal: 30.0),
                    child: BlocBuilder<ShoesBloc, ShoesState>(
                        builder: (context, state) {
                      if (state is GetListShoesOnSuccess) {
                        var params = 'Nouveautés ';
                        if (state.currentParam.isNotEmpty) {
                          params += paramsFiltered(
                              state.currentParam['filtersGender'],
                              'filtersGender',
                              ' - ');
                          params +=
                              " ${paramsFiltered(state.currentParam['filtersPrice'], 'filtersPrice', ' - ')}";
                          params +=
                              " ${paramsFiltered(state.currentParam['filtersColor'], 'filtersColor', ' - ')}";
                          params +=
                              " ${paramsFiltered(state.currentParam['filtersSport'], 'filtersSport', ' - ')}";
                        }
                        return Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: isWeb
                                  ? (screenSize <= 600)
                                      ? 15.0
                                      : 10.0
                                  : 10.0,
                              horizontal: isWeb
                                  ? (screenSize <= 600)
                                      ? 20.0
                                      : 0.0
                                  : 0.0),
                          child: Row(
                            mainAxisAlignment: isWeb
                                ? (screenSize <= 600)
                                    ? MainAxisAlignment.start
                                    : MainAxisAlignment.spaceBetween
                                : MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0),
                                  child: Text(
                                    "$params(${state.shoesList.length})",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: isWeb
                                            ? (screenSize <= 600)
                                                ? 26.0
                                                : 18.0
                                            : 24.0),
                                  ),
                                ),
                              ),
                              isWeb
                                  ? (screenSize <= 600)
                                      ? InkWell(
                                          onTap: () {
                                            context
                                                .read<FiltersBloc>()
                                                .add(OnFilterByGenderInit());
                                            Navigator.pushNamed(
                                                context, "/filterMobile");
                                          },
                                          child: const Icon(
                                            Icons.compare_arrows,
                                            size: 30.0,
                                          ),
                                        )
                                      : const SizedBox()
                                  : InkWell(
                                      onTap: () {
                                        context
                                            .read<FiltersBloc>()
                                            .add(OnFilterByGenderInit());
                                        Navigator.pushNamed(
                                            context, "/filterMobile");
                                      },
                                      child: const Icon(
                                        Icons.compare_arrows,
                                        size: 30.0,
                                      ),
                                    )
                            ],
                          ),
                        );
                      } else {
                        return const Text("Nouveautés");
                      }
                    }),
                  ),
                  isWeb
                      ? (screenSize <= 600)
                          ? Flexible(
                              child: ScrollConfiguration(
                                  behavior: ScrollConfiguration.of(context)
                                      .copyWith(scrollbars: false),
                                  child:
                                      ListView(children: const [ShoesList()])))
                          : Flexible(
                              child: ScrollConfiguration(
                                behavior: ScrollConfiguration.of(context)
                                    .copyWith(scrollbars: false),
                                child: ListView(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                8,
                                            height: MediaQuery.of(context)
                                                .size
                                                .height,
                                            child: ListView(
                                              physics:
                                                  const NeverScrollableScrollPhysics(),
                                              children: const [
                                                Divider(),
                                                FilterByGender(),
                                                Divider(),
                                                FilterByPrice(),
                                                Divider(),
                                                FilterByColor(),
                                                Divider(),
                                                FilterBySport(),
                                              ],
                                            )),
                                        const SizedBox(width: 20.0),
                                        const Flexible(child: ShoesList())
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            )
                      : Flexible(
                          child: ScrollConfiguration(
                              behavior: ScrollConfiguration.of(context)
                                  .copyWith(scrollbars: false),
                              child: ListView(children: [ShoesList()]))),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  String paramsFiltered(value, key, chartJoin) {
    return value != null && value.length > 0
        ? value
            .map((word) => word[0].toUpperCase() + word.substring(1))
            .join(chartJoin)
        : '';
  }
}
