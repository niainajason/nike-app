import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/core/constants/storage.dart';
import 'package:untitled1/domain/entities/filter_by_color_entity.dart';
import 'package:untitled1/domain/entities/filter_by_gender_entity.dart';
import 'package:untitled1/domain/entities/filter_by_price.dart';
import 'package:untitled1/domain/entities/filter_by_sport_entity.dart';
import 'package:untitled1/presentation/filters/bloc/filter_bloc.dart';
import 'package:untitled1/presentation/filters/bloc/filters_event.dart';
import 'package:untitled1/presentation/filters/bloc/filters_state.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_bloc.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_event.dart';
import 'package:untitled1/presentation/views/widgets/filter_by_color.dart';
import 'package:untitled1/presentation/views/widgets/filter_by_gender.dart';
import 'package:untitled1/presentation/views/widgets/filter_by_price.dart';
import 'package:untitled1/presentation/views/widgets/filter_by_sport.dart';

class FilterMobile extends StatefulWidget {
  const FilterMobile({Key? key}) : super(key: key);

  @override
  State<FilterMobile> createState() => _FilterMobileState();
}

class _FilterMobileState extends State<FilterMobile> {
  var isWeb = kIsWeb;
  // List<FilterByGenderEntity> filtersGender = [];
  // List<FilterByPriceEntity> filtersPrice = [];
  // List<FilterBySportEntity> filtersSport = [];
  // List<FilterByColorEntity> filtersColor = [];
  Map<String, List> params = {
    'filtersGender': genderFilterItems,
    'filtersPrice': priceFilterItems,
    'filtersSport': sportFilterItems,
    'filtersColor': colorFilterItems
  };
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: ScrollConfiguration(
        behavior: ScrollConfiguration.of(context).copyWith(scrollbars: false),
        child: Column(children: [
          BlocListener<FiltersBloc, FilterState>(
            listener: (context, state) {
              if (state is UpdateFilterByGenderOnSuccess) {
                params = {
                  'filtersGender':
                      state.genderFilters as List<FilterByGenderEntity>,
                  'filtersPrice':
                      state.priceFilters as List<FilterByPriceEntity>,
                  'filtersSport':
                      state.sportFilters as List<FilterBySportEntity>,
                  'filtersColor':
                      state.colorFilters as List<FilterByColorEntity>
                };
              }
            },
            child: Container(
              height: 50.0,
              width: double.infinity,
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: InkWell(
                  onTap: () {
                    context.read<FiltersBloc>().add(OnFilterByGenderInit());
                    context.read<ShoesBloc>().add(ShoesInit());
                    Navigator.pushNamed(context, "/dashboard");
                  },
                  child: Container(
                      height: 40,
                      width: 40,
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle, color: Colors.black),
                      child: const Icon(Icons.close,
                          size: 24, color: Colors.white)),
                ),
              ),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: ListView(
                children: const [
                  Divider(),
                  SizedBox(
                    height: 20.0,
                  ),
                  FilterByGender(),
                  Divider(),
                  SizedBox(
                    height: 20.0,
                  ),
                  FilterByPrice(),
                  Divider(),
                  SizedBox(
                    height: 20.0,
                  ),
                  FilterByColor(),
                  Divider(),
                  SizedBox(
                    height: 20.0,
                  ),
                  FilterBySport(),
                ],
              ),
            ),
          ),
          Container(
            height: 100.0,
            width: double.infinity,
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () {
                    context.read<FiltersBloc>().add(OnFilterByGenderInit());
                    context.read<ShoesBloc>().add(ShoesInit());
                  },
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)),
                      padding: const EdgeInsets.all(0.0),
                      elevation: 4,
                      textStyle: const TextStyle(color: Colors.white)),
                  child: Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      height: 50.0,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(80.0)),
                      ),
                      child: Center(
                          child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: BlocBuilder<FiltersBloc, FilterState>(
                                  builder: (context, state) {
                                if (state is UpdateFilterByGenderOnSuccess) {
                                  var checkedGenderCount = state.genderFilters
                                          ?.where((element) =>
                                              element.isChecked == true)
                                          .length ??
                                      0;
                                  var checkedPriceCount = state.priceFilters
                                          ?.where((element) =>
                                              element.isChecked == true)
                                          .length ??
                                      0;
                                  var checkedColorCount = state.colorFilters
                                          ?.where((element) =>
                                              element.isChecked == true)
                                          .length ??
                                      0;
                                  var checkedSportCount = state.sportFilters
                                          ?.where((element) =>
                                              element.isChecked == true)
                                          .length ??
                                      0;
                                  var total = checkedGenderCount +
                                      checkedPriceCount +
                                      checkedColorCount +
                                      checkedSportCount;
                                  var totalText = total != 0 ? '($total)' : '';
                                  return Text(
                                    "Effacer  $totalText",
                                    style: const TextStyle(
                                        color: Colors.black, fontSize: 18.0),
                                  );
                                } else {
                                  return const Text("Effacer");
                                }
                              })))),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                ElevatedButton(
                  onPressed: () {
                    context
                        .read<ShoesBloc>()
                        .add(OnShoesFilter(params: params));
                    Navigator.pushNamed(context, "/dashboard");
                  },
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)),
                      padding: const EdgeInsets.all(0.0),
                      elevation: 4,
                      textStyle: const TextStyle(color: Colors.white)),
                  child: Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      height: 50.0,
                      decoration: const BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.all(Radius.circular(80.0)),
                      ),
                      child: const Center(
                          child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          "Appliquer",
                          style: TextStyle(color: Colors.white, fontSize: 18.0),
                        ),
                      ))),
                ),
              ],
            ),
          ),
        ]),
      ),
    ));
  }
}
