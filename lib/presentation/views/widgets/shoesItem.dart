import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:untitled1/domain/entities/shoes_entity.dart';
import 'package:flutter/foundation.dart';

Column shoesItem({required ShoesEntity shoesEntity, BuildContext? context}) {
  var isWeb = kIsWeb;
  final image = shoesEntity.image;
  final screenSize = MediaQuery.of(context!).size.width;
  return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Flexible(
          child: Container(
            color: Color(int.parse('0xFFf6f6f6')),
            child: Image.asset(
              'assets/img/${image!}',
              width: double.infinity,
              fit: BoxFit.scaleDown,
              height: (screenSize <= 600) ? 200 : 320,
            ),
          ),
        ),
        const SizedBox(height: 5.0),
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: isWeb
                  ? (screenSize <= 600)
                      ? 10.0
                      : 5.0
                  : 5.0,
              vertical: 3.0),
          child: Text(
            shoesEntity.article ?? "No title",
            maxLines: 1,
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 14.0),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: isWeb
                  ? (screenSize <= 600)
                      ? 10.0
                      : 5.0
                  : 5.0,
              vertical: 3.0),
          child: Text(
            "Chaussure pour ${shoesEntity.gender ?? ""}",
            style: const TextStyle(color: Colors.grey, fontSize: 12.0),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: isWeb
                  ? (screenSize <= 600)
                      ? 10.0
                      : 5.0
                  : 5.0,
              vertical: 3.0),
          child: Text(
            '${shoesEntity.color?.split(',').length} Couleur(s)',
            style: const TextStyle(color: Colors.grey, fontSize: 12.0),
          ),
        ),
        const SizedBox(
          height: 6.0,
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: isWeb
                  ? (screenSize <= 600)
                      ? 10.0
                      : 5.0
                  : 5.0,
              vertical: 3.0),
          child: Text(
            '${shoesEntity.price} €',
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 14.0),
          ),
        ),
        isWeb
            ? const SizedBox()
            : const SizedBox(
                height: 20.0,
              ),
      ]);
}
