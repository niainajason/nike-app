import 'package:flutter/material.dart';

class GlobalPage extends StatelessWidget {
  GlobalPage(
      {Key? key, required this.child, this.leading, this.showAppBar = true})
      : super(key: key);

  final Widget child;

  final dynamic leading;

  final bool showAppBar;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: child,
    );
  }
}

Widget? Function(BuildContext, String) placeholderWidgetFn() =>
    (_, s) => placeholderWidget();

Widget placeholderWidget() => Icon(Icons.image);
