import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/core/constants/storage.dart';
import 'package:untitled1/presentation/filters/bloc/filter_bloc.dart';
import 'package:untitled1/presentation/filters/bloc/filters_event.dart';
import 'package:untitled1/presentation/filters/bloc/filters_state.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_bloc.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_event.dart';

class FilterByColor extends StatefulWidget {
  const FilterByColor({Key? key}) : super(key: key);

  @override
  State<FilterByColor> createState() => _FilterByColorState();
}

class _FilterByColorState extends State<FilterByColor> {
  var isWeb = kIsWeb;
  bool isOpen = true;
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size.width;
    return BlocBuilder<FiltersBloc, FilterState>(builder: (context, state) {
      if (state is UpdateFilterByGenderOnSuccess) {
        var checkedCount = state.colorFilters
            ?.where((element) => element.isChecked == true)
            .length;
        return Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Couleur${checkedCount != 0 ? '($checkedCount)' : ''}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: isWeb ? 14.0 : 16.0),
                    ),
                    if (isWeb && screenSize > 600)
                      InkWell(
                          onTap: () {
                            setState(() {
                              isOpen = !isOpen;
                            });
                          },
                          child: isOpen
                              ? const Icon(Icons.arrow_drop_up_outlined)
                              : const Icon(Icons.arrow_drop_down_outlined)),
                  ],
                ),
              ),
              if (isOpen)
                const SizedBox(
                  height: 15.0,
                ),
              if (isOpen || !isWeb)
                Container(
                    // color: Colors.red,
                    alignment: Alignment.centerLeft,
                    // width: 170.0,
                    child: BlocBuilder<FiltersBloc, FilterState>(
                        builder: (context, state) {
                      if (state is UpdateFilterByGenderOnSuccess) {
                        return GridView.count(
                            shrinkWrap: true,
                            primary: false,
                            padding: const EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 2.0),
                            crossAxisSpacing: 1,
                            mainAxisSpacing: 1,
                            crossAxisCount: 3,
                            children: state.colorFilters?.map((e) {
                                  String color =
                                      e.color?.replaceAll('#', '0xff') ??
                                          '0xfffff';
                                  return Column(
                                    children: [
                                      Flexible(
                                        child: InkWell(
                                          onTap: () {
                                            e.isChecked = !e.isChecked!;
                                            context.read<FiltersBloc>().add(
                                                OnFilter(
                                                    filterGender:
                                                        state.genderFilters!,
                                                    filtersPrice:
                                                        state.priceFilters!,
                                                    filtersSport:
                                                        state.sportFilters!,
                                                    filterColor:
                                                        state.colorFilters!));
                                            var params = {
                                              'filtersGender':
                                                  state.genderFilters!,
                                              'filtersPrice':
                                                  state.priceFilters!,
                                              'filtersSport':
                                                  state.sportFilters!,
                                              'filtersColor':
                                                  state.colorFilters!
                                            };
                                            if (isWeb) {
                                              context.read<ShoesBloc>().add(
                                                  OnShoesFilter(
                                                      params: params));
                                            }
                                          },
                                          child: Container(
                                            width: isWeb
                                                ? (screenSize <= 600)
                                                    ? 40
                                                    : 30
                                                : 40,
                                            height: isWeb
                                                ? (screenSize <= 600)
                                                    ? 40
                                                    : 30
                                                : 40,
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Color(int.parse(color))),
                                            child: e.isChecked
                                                ? Icon(
                                                    Icons.check,
                                                    size: isWeb ? 16.0 : 18.0,
                                                    color: e.value == 'blanc'
                                                        ? Colors.grey
                                                        : Colors.white,
                                                  )
                                                : const SizedBox(),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5.0,
                                      ),
                                      Flexible(
                                        child: Text(
                                          e.title ?? '',
                                          style: TextStyle(
                                              fontSize: isWeb ? 10.0 : 13.0),
                                        ),
                                      )
                                    ],
                                  );
                                }).toList() ??
                                []);
                      } else {
                        return const Text("Pas de choix de couleur");
                      }
                    })),
              isOpen
                  ? screenSize <= 600
                      ? const SizedBox(height: 20.0)
                      : SizedBox()
                  : screenSize <= 600
                      ? const SizedBox(height: 20.0)
                      : SizedBox()
            ],
          ),
        );
      } else {
        return const Text("Pas de choix de couleur");
      }
    });
  }
}
