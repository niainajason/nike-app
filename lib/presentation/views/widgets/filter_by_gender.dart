import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/core/constants/storage.dart';
import 'package:untitled1/presentation/filters/bloc/filter_bloc.dart';
import 'package:untitled1/presentation/filters/bloc/filters_event.dart';
import 'package:untitled1/presentation/filters/bloc/filters_state.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_bloc.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_event.dart';

class FilterByGender extends StatefulWidget {
  const FilterByGender({Key? key}) : super(key: key);

  @override
  State<FilterByGender> createState() => _FilterByGenderState();
}

class _FilterByGenderState extends State<FilterByGender> {
  var isWeb = kIsWeb;
  bool isOpen = true;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size.width;
    return BlocBuilder<FiltersBloc, FilterState>(builder: (context, state) {
      if (state is UpdateFilterByGenderOnSuccess) {
        var checkedCount = state.genderFilters
            ?.where((element) => element.isChecked == true)
            .length;
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "$searchByGender ${checkedCount != 0 ? '($checkedCount)' : ''}",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: isWeb ? 14.0 : 16.0),
                  ),
                  if (isWeb && screenSize > 600)
                    InkWell(
                        onTap: () {
                          setState(() {
                            isOpen = !isOpen;
                          });
                        },
                        child: isOpen
                            ? const Icon(Icons.arrow_drop_up_outlined)
                            : const Icon(Icons.arrow_drop_down_outlined)),
                ],
              ),
            ),
            if (isOpen)
              const SizedBox(
                height: 15.0,
              ),
            if (isOpen || !isWeb)
              Flexible(
                  child: Column(
                      children: state.genderFilters?.map((e) {
                            var index = genderFilterItems.indexOf(e);
                            return Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: isWeb ? 2.5 : 4.0),
                              child: Row(
                                children: [
                                  SizedBox(
                                    height: 20.0,
                                    width: 20.0,
                                    child: Checkbox(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(3.0),
                                      ),
                                      side: MaterialStateBorderSide.resolveWith(
                                        (states) => BorderSide(
                                            width: 1.0,
                                            color: Colors.grey.shade500),
                                      ),
                                      activeColor: Colors.black,
                                      checkColor: Colors.white,
                                      hoverColor: Colors.transparent,
                                      value: e.isChecked,
                                      onChanged: (value) {
                                        e?.isChecked = value!;
                                        context.read<FiltersBloc>().add(
                                            OnFilter(
                                                filterGender:
                                                    state.genderFilters!,
                                                filtersPrice:
                                                    state.priceFilters!,
                                                filtersSport:
                                                    state.sportFilters!,
                                                filterColor:
                                                    state.colorFilters!));
                                        var params = {
                                          'filtersGender': state.genderFilters!,
                                          'filtersPrice': state.priceFilters!,
                                          'filtersSport': state.sportFilters!,
                                          'filtersColor': state.colorFilters!
                                        };
                                        if (isWeb) {
                                          context.read<ShoesBloc>().add(
                                              OnShoesFilter(params: params));
                                        }
                                      },
                                    ),
                                  ),
                                  const SizedBox(width: 3.0),
                                  Flexible(
                                    child: Text(
                                      e.title!,
                                      style: TextStyle(
                                          fontSize: isWeb ? 14.0 : 16.0),
                                    ),
                                  )
                                ],
                              ),
                            );
                          }).toList() ??
                          [])),
            isOpen
                ? screenSize <= 600
                    ? const SizedBox(height: 20.0)
                    : SizedBox()
                : screenSize <= 600
                    ? const SizedBox(height: 20.0)
                    : SizedBox()
          ],
        );
      } else {
        return const Text("Pas de filtre");
      }
    });
  }
}
