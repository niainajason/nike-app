import 'package:flutter/material.dart';

List<Widget> checkboxFilter(items) {
  List<Widget> widgets = [];
  for (var e in items) {
    var index = items.indexOf(e);
    widgets.add(checkBoxWithTitle(
      e['title'],
      e['isChecked'],
    ));
  }
  return widgets;
}

Widget checkBoxWithTitle(title, isChecked) {
  return Row(
    children: [
      Checkbox(
          value: isChecked,
          onChanged: (value) {
            print(value);
            print("Here I changed");
          }),
      Text(
        title,
        style: const TextStyle(fontSize: 14.0),
      )
    ],
  );
}
