import 'package:equatable/equatable.dart';
import 'package:untitled1/domain/entities/filter_by_gender_entity.dart';
import 'package:untitled1/domain/entities/shoes_entity.dart';

abstract class ShoesEvent extends Equatable {
  const ShoesEvent();
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class ShoesInit extends ShoesEvent {}

class OnGetShoes extends ShoesEvent {
  Object params;
  OnGetShoes({required this.params});

  @override
  // TODO: implement props
  List<Object?> get props => [params];
}

class OnShoesFilter extends ShoesEvent {
  Map<String, List> params;
  OnShoesFilter({required this.params});

  @override
  // TODO: implement props
  List<Object?> get props => [params];
}
