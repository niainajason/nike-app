import 'dart:convert';
import 'dart:developer' as developer;
import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/core/constants/storage.dart';
import 'package:untitled1/domain/entities/filter_by_color_entity.dart';
import 'package:untitled1/domain/entities/filter_by_gender_entity.dart';
import 'package:untitled1/domain/entities/filter_by_price.dart';
import 'package:untitled1/domain/entities/filter_by_sport_entity.dart';
import 'package:untitled1/domain/entities/shoes_entity.dart';
import 'package:untitled1/domain/usecases/shoes/get_list_usecase.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_event.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_state.dart';

class ShoesBloc extends Bloc<ShoesEvent, ShoesState> {
  final GetListUseCase getListUseCase;
  final filterByGender = genderFilterItems;
  ShoesBloc({required this.getListUseCase}) : super(ShoesLoading()) {
    on<ShoesInit>(_onInitShoes);
    on<OnGetShoes>(_onGetShoes);
    on<OnShoesFilter>(_onShoesFilter);
  }

  Future<void> _onInitShoes(ShoesInit event, Emitter<ShoesState> emit) async {
    emit(ShoesLoading());
    var data = await getListUseCase({});
    developer.log(data.tryGetSuccess()!.toString(), name: "RESULT");
    List<FilterByGenderEntity> filtersGender = [];
    List<FilterByPriceEntity> filtersPrice = [];
    List<FilterBySportEntity> filtersSport = [];
    List<FilterByColorEntity> filtersColor = [];
    var params = {
      'filtersGender': filtersGender,
      'filtersPrice': filtersPrice,
      'filtersSport': filtersSport,
      'filtersColor': filtersColor
    };
    emit(GetListShoesOnSuccess(
        shoesList: data.tryGetSuccess()!, currentParam: params));
  }

  Future<void> _onGetShoes(OnGetShoes event, Emitter<ShoesState> emit) async {
    emit(ShoesLoading());
    var data = await getListUseCase({});
    emit(GetListShoesOnSuccess(
        shoesList: data.tryGetSuccess()!, currentParam: {}));
    // add(ShoesInit());
  }

  Future<void> _onShoesFilter(
      OnShoesFilter event, Emitter<ShoesState> emit) async {
    emit(ShoesLoading());
    var result = await getListUseCase({});
    var data = result.tryGetSuccess();
    var params = event.params;
    var filtersGenderValues =
        getCheckedValues(params['filtersGender'] as List<FilterByGenderEntity>);
    var filtersSportValues =
        getCheckedValues(params['filtersSport'] as List<FilterBySportEntity>);
    var filtersColorValues =
        getCheckedValues(params['filtersColor'] as List<FilterByColorEntity>);
    var filtersPriceValues =
        getPriceInterval(params['filtersPrice'] as List<FilterByPriceEntity>);

    Map<String, dynamic> currentParam = {};

    List<ShoesEntity>? dataResult = data;
    if (filtersGenderValues.length > 0) {
      dataResult = dataResult
          ?.where((element) => filtersGenderValues.contains(element.gender))
          .toList();
      currentParam['filtersGender'] = filtersGenderValues;
    }
    if (filtersSportValues.length > 0) {
      dataResult = dataResult
          ?.where((element) => filtersSportValues.contains(element.sport))
          .toList();
      currentParam['filtersSport'] = filtersSportValues;
    }
    if (filtersColorValues.length > 0) {
      dataResult = dataResult
          ?.where((element) =>
              element.color
                  ?.toLowerCase()
                  ?.split(",")
                  .any((e) => filtersColorValues.contains(e.trim())) ??
              false)
          .toList();
      currentParam['filtersColor'] = filtersColorValues;
    }
    if (filtersPriceValues.length > 0) {
      List<int> values = [];
      for (List sublist in filtersPriceValues) {
        values.addAll(
            sublist.map((e) => e is int ? e : 0).where((e) => e != null));
      }
      int maxValue = values.reduce(max);
      int minValue = values.reduce(min);
      if (minValue == -50 && maxValue == -50) {
        dataResult =
            dataResult?.where((element) => element.price! < 50).toList();
        currentParam['filtersPrice'] = ["Moins €50"];
      } else if (minValue == 150 && maxValue == 150) {
        dataResult =
            dataResult?.where((element) => element.price! > maxValue).toList();
        currentParam['filtersPrice'] = ["Plus de €150"];
      } else if (minValue != maxValue) {
        dataResult = dataResult
            ?.where((element) =>
                element.price! < maxValue && element.price! > minValue)
            .toList();
        if (minValue == -50) {
          currentParam['filtersPrice'] = ["Moins €50", "€$maxValue"];
        } else {
          currentParam['filtersPrice'] = ["€$minValue", "€$maxValue"];
        }
      }
    }
    emit(GetListShoesOnSuccess(
        shoesList: dataResult!, currentParam: currentParam));
  }

  getCheckedValues(filters) {
    var filtersValues = filters
        .where((element) => element.isChecked == true)
        .map((element) => element.value)
        .toList();
    return filtersValues;
  }

  getPriceInterval(filters) {
    var filtersValues = filters
        .where((element) => element.isChecked == true)
        .map((element) => element.price)
        .toList();
    return filtersValues;
  }
}
