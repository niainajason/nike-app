import 'package:equatable/equatable.dart';
import 'package:untitled1/domain/entities/filter_by_gender_entity.dart';
import 'package:untitled1/domain/entities/shoes_entity.dart';

abstract class ShoesState extends Equatable {
  const ShoesState();
  @override
  List<Object?> get props => [];
}

class ShoesLoading extends ShoesState {}

class GetListShoesOnSuccess extends ShoesState {
  List<ShoesEntity> shoesList;
  final Map<String, dynamic> currentParam;
  GetListShoesOnSuccess({required this.shoesList, required this.currentParam});
  @override
  List<Object?> get props => [shoesList, currentParam];
}

class UpdateShoesListOnSuccess extends ShoesState {
  List<FilterByGenderEntity> filters;
  UpdateShoesListOnSuccess({required this.filters});
  @override
  List<Object?> get props => [filters];
}

class CurrentFiltersParam extends ShoesState {
  Map<String, dynamic> params;
  CurrentFiltersParam({required this.params});
  @override
  List<Object?> get props => [params];
}
