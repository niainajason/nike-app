import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_bloc.dart';
import 'package:untitled1/presentation/shoes/bloc/shoes_state.dart';
import 'package:untitled1/presentation/views/widgets/shoesItem.dart';

class ShoesList extends StatefulWidget {
  const ShoesList({Key? key}) : super(key: key);

  @override
  State<ShoesList> createState() => _ShoesListState();
}

class _ShoesListState extends State<ShoesList> {
  var isWeb = kIsWeb;
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size.width;
    return BlocBuilder<ShoesBloc, ShoesState>(
      builder: (context, state) {
        if (state is ShoesLoading) {
          return const Text("Shoes on loading");
        } else if (state is GetListShoesOnSuccess) {
          if (state.shoesList.isNotEmpty) {
            return GridView.count(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              primary: false,
              padding:
                  const EdgeInsets.symmetric(vertical: 0.0, horizontal: 2.0),
              crossAxisSpacing: 10,
              mainAxisSpacing: 0,
              crossAxisCount: isWeb
                  ? (screenSize <= 600)
                      ? 2
                      : 3
                  : 2,
              childAspectRatio: isWeb
                  ? (screenSize <= 600)
                      ? 0.72
                      : 0.7
                  : 0.6,
              children: listShoes(state.shoesList, context),
            );
          } else {
            return const Center(
              child: Text("Aucun résultat"),
            );
          }
        } else {
          return Text(state.toString());
        }
      },
    );
  }
}

List<Widget> listShoes(items, context) {
  List<Widget> widgets = [];
  for (var e in items) {
    widgets.add(shoesItem(shoesEntity: e, context: context));
  }
  return widgets;
}
