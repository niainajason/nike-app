import 'package:equatable/equatable.dart';
import 'package:untitled1/domain/entities/filter_by_color_entity.dart';
import 'package:untitled1/domain/entities/filter_by_gender_entity.dart';
import 'package:untitled1/domain/entities/filter_by_price.dart';
import 'package:untitled1/domain/entities/filter_by_sport_entity.dart';

abstract class FiltersEvent extends Equatable {
  const FiltersEvent();
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class OnFilterByGenderInit extends FiltersEvent {}

class OnFilter extends FiltersEvent {
  List<FilterByGenderEntity> filterGender;
  List<FilterByPriceEntity> filtersPrice;
  List<FilterBySportEntity> filtersSport;
  List<FilterByColorEntity> filterColor;
  OnFilter(
      {required this.filterGender,
      required this.filtersPrice,
      required this.filtersSport,
      required this.filterColor});

  @override
  // TODO: implement props
  List<Object?> get props =>
      [filterGender, filtersPrice, filtersSport, filterColor];
}

class OnFilterByPrice extends FiltersEvent {
  List<FilterByPriceEntity> filters;
  OnFilterByPrice({required this.filters});

  @override
  // TODO: implement props
  List<Object?> get props => [filters];
}
