import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled1/core/constants/storage.dart';
import 'package:untitled1/domain/entities/filter_by_color_entity.dart';
import 'package:untitled1/domain/entities/filter_by_gender_entity.dart';
import 'package:untitled1/domain/entities/filter_by_price.dart';
import 'package:untitled1/domain/entities/filter_by_sport_entity.dart';
import 'package:untitled1/presentation/filters/bloc/filters_event.dart';
import 'package:untitled1/presentation/filters/bloc/filters_state.dart';

class FiltersBloc extends Bloc<FiltersEvent, FilterState> {
  final filterByGender = genderFilterItems;
  final filterByPrice = priceFilterItems;
  final filterBySport = sportFilterItems;
  final filterByColor = colorFilterItems;
  FiltersBloc() : super(FilterLoading()) {
    on<OnFilterByGenderInit>(_onFilterInit);
    on<OnFilter>(_onFilter);
  }

  Future<void> _onFilterInit(
      OnFilterByGenderInit event, Emitter<FilterState> emit) async {
    List<FilterByPriceEntity> prices = priceFilterItems;
    emit(FilterLoading());
    List<FilterByGenderEntity> genderFilterItemsOrigin = [
      FilterByGenderEntity(title: "Hommes", value: "Homme", isChecked: false),
      FilterByGenderEntity(title: "Femmes", value: "Femme", isChecked: false),
      FilterByGenderEntity(title: "Mixte", value: "Mixte", isChecked: false)
    ];
    List<FilterByPriceEntity> priceFilterItemsOrigin = [
      FilterByPriceEntity(title: "Moins €50", price: [-50], isChecked: false),
      FilterByPriceEntity(
          title: "€50 - €100", price: [50, 100], isChecked: false),
      FilterByPriceEntity(
          title: "€100 - €150", price: [100, 150], isChecked: false),
      FilterByPriceEntity(
          title: "Plus de €150", price: [150], isChecked: false),
    ];
    List<FilterBySportEntity> sportFilterItemsOrigin = [
      FilterBySportEntity(
          title: "Football", value: 'Football', isChecked: false),
      FilterBySportEntity(title: "Basket", value: 'Basket', isChecked: false),
      FilterBySportEntity(title: "Running", value: 'Running', isChecked: false)
    ];
    List<FilterByColorEntity> colorFilterItemsOrigin = [
      FilterByColorEntity(
          title: "Noir", color: '#000000', value: 'noir', isChecked: false),
      FilterByColorEntity(
          title: "Rouge", color: "#FF0000", value: 'rouge', isChecked: false),
      FilterByColorEntity(
          title: "Blanc", color: "#fff", value: 'blanc', isChecked: false),
      FilterByColorEntity(
          title: "Jaune", color: "#FFFF00", value: 'jaune', isChecked: false),
      FilterByColorEntity(
          title: "Vert", color: "#008000", value: 'vert', isChecked: false),
      FilterByColorEntity(
          title: "Bleu", color: "#0000FF", value: 'bleu', isChecked: false),
      FilterByColorEntity(
          title: "Rose", color: "#fd6c9e", value: 'rose', isChecked: false),
      FilterByColorEntity(
          title: "Gris", color: "#808080", value: 'gris', isChecked: false),
    ];
    emit(UpdateFilterByGenderOnSuccess(
        genderFilters: genderFilterItemsOrigin,
        priceFilters: priceFilterItemsOrigin,
        sportFilters: sportFilterItemsOrigin,
        colorFilters: colorFilterItemsOrigin));
  }

  Future<void> _onFilter(OnFilter event, Emitter<FilterState> emit) async {
    emit(OnUpdateFilter());
    emit(UpdateFilterByGenderOnSuccess(
      genderFilters: event.filterGender,
      priceFilters: event.filtersPrice,
      sportFilters: event.filtersSport,
      colorFilters: event.filterColor,
    ));
  }
}
