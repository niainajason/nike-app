import 'package:equatable/equatable.dart';
import 'package:untitled1/domain/entities/filter_by_color_entity.dart';
import 'package:untitled1/domain/entities/filter_by_gender_entity.dart';
import 'package:untitled1/domain/entities/filter_by_price.dart';
import 'package:untitled1/domain/entities/filter_by_sport_entity.dart';

abstract class FilterState extends Equatable {
  const FilterState();
  @override
  List<Object?> get props => [];
}

class FilterLoading extends FilterState {}

class UpdateFilterByGenderOnSuccess extends FilterState {
  List<FilterByGenderEntity>? genderFilters;
  List<FilterByPriceEntity>? priceFilters;
  List<FilterBySportEntity>? sportFilters;
  List<FilterByColorEntity>? colorFilters;
  UpdateFilterByGenderOnSuccess(
      {this.genderFilters,
      this.priceFilters,
      this.sportFilters,
      this.colorFilters});
  @override
  List<Object?> get props =>
      [genderFilters, priceFilters, sportFilters, colorFilters];
}

class OnUpdateFilter extends FilterState {}
